import React from "react";
import logo from "./logo.svg";
import "./App.css";
// import MaterialTableDemo from "./Components/DataTable/DataTable";
// import Login from "./Containers/Login/Login";
import Routes from "./Routes";

import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function App() {
  return (
    <div className="App">
      {/* <MaterialTableDemo /> */}
      {/* <Login /> */}
      <Routes />
      <ToastContainer />
    </div>
  );
}

export default App;
