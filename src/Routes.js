import React, { lazy, Suspense } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { createBrowserHistory } from "history";

const history = createBrowserHistory();

const Login = lazy(() => import("./Containers/Login/Login"));
const UserCategory = lazy(() => import("./Components/DataTable/DataTable"));
const Dashboard = lazy(() => import("./Containers/Dashboard/Dashboard"));
const CMS = lazy(() => import("./Containers/CMS/CMS"));

class Routes extends React.Component {
  render() {
    return (
      <Router history={history}>
        <Suspense fallback={<div>Loading...</div>}>
          <Route exact path="/" component={Login} />
          <Route path="/UserCategory" component={UserCategory} />
          <Route path="/Dashboard" component={Dashboard} />
          <Route path="/CMS" component={CMS} />
        </Suspense>
      </Router>
    );
  }
}

export default Routes;
