import React from "react";

import AdminGrid from "../../Components/AdminGrid/AdminGrid";

class Dashboard extends React.Component {
  render() {
    return (
      <AdminGrid>
        <h2>Dashboard</h2>
      </AdminGrid>
    );
  }
}

export default Dashboard;
