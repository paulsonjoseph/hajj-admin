import React, { useState, useEffect, useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { createBrowserHistory } from "history";

import FilledInput from "@material-ui/core/FilledInput";
import FormControl from "@material-ui/core/FormControl";
import FormHelperText from "@material-ui/core/FormHelperText";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";

import { Loginapi } from "../../api/loginapi";

const history = createBrowserHistory({ forceRefresh: true });

const useStyles = makeStyles(theme => ({
  loginblk: {
    display: "flex",
    flexWrap: "wrap",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    minHeight: "100vh",
    background: "#eeeeee"
  },
  container: {
    display: "flex",
    flexWrap: "wrap",
    flexDirection: "column",
    width: "100%",
    maxWidth: "300px",
    boxShadow: "3px 3px 8px rgba(0,0,0,0.16)",
    padding: "15px 30px 30px",
    borderRadius: "4px",
    background: "#ffffff"
  },
  button: {
    marginTop: "20px"
  }
}));

export default function Login() {
  const classes = useStyles();

  const [state, setState] = React.useState({
    userName: "",
    password: ""
  });
  const [loggedin, setLoggedin] = useState(false);

  const handleChange = (event, field) => {
    // this.setState({ [field]: event.target.value });
    setState({ ...state, [field]: event.target.value });
  };

  const handleSubmit = event => {
    event.preventDefault();
    Loginapi(state);
  };

  return (
    <div className={classes.loginblk}>
      <form
        className={classes.container}
        noValidate
        autoComplete="off"
        onSubmit={handleSubmit}
      >
        <TextField
          label="Username"
          className={classes.textField}
          value={state.userName}
          onChange={e => handleChange(e, "userName")}
          margin="normal"
          required
        />
        <TextField
          label="Password"
          className={classes.textField}
          value={state.password}
          onChange={e => handleChange(e, "password")}
          margin="normal"
          type="password"
          required
        />
        <Button
          variant="contained"
          type="submit"
          color="primary"
          className={classes.button}
        >
          Login
        </Button>
      </form>
    </div>
  );
}
