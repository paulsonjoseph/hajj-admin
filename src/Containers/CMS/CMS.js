import React from "react";

import AdminGrid from "../../Components/AdminGrid/AdminGrid";
import DataTable from "../../Components/DataTable/DataTable";
import { cmsListApi } from "../../api/cms/cmsListApi";

class CMS extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      columns: [
        { title: "Name", field: "title" },
        { title: "UrlKey", field: "urlKey" },
        { title: "Status", field: "status" }
      ]
    };
  }
  componentDidMount() {
    cmsListApi().then(list => {
      console.log("data", list);
      this.setState({ data: list });
    });
  }
  componentDidUpdate() {
    console.log(this.state.data);
  }
  render() {
    return (
      <AdminGrid>
        <h2>CMS</h2>
        <DataTable
          data={this.state.data}
          title="CMS List"
          columns={this.state.columns}
        />
      </AdminGrid>
    );
  }
}

export default CMS;
