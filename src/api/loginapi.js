import { createBrowserHistory } from "history";
import { toast } from "react-toastify";

const history = createBrowserHistory({ forceRefresh: true });

export const Loginapi = state => {
  fetch("https://hajj-api.dev.ispgnet.com/api/user/v1/admin/login", {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(state)
  })
    .then(response => {
      return response.json();
    })
    .then(json => {
      if (json.token) {
        localStorage.setItem("token", json.token);
        toast.success("Logged in successfully.");
        history.push("/Dashboard");
      } else {
        toast.error(json.message);
      }
    });
};
