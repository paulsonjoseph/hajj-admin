import axios from "axios";
import { API_DOMAIN } from "../../apiDomain";

const headers = {
  "Content-Type": "application/json",
  Authorization: `Bearer ${localStorage.getItem("token")}`,
  "Content-Language": "en"
};

export const cmsListApi = state => {
  return axios.get(`${API_DOMAIN}/api/cmspage/v1`, { headers }).then(res => {
    const response = res.data;
    // console.log(cmslist);
    return response;
  });
};
