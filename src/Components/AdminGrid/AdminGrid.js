import React from "react";

import Header from "../Header/Header";
import Menu from "../Menu/Menu";

class CMS extends React.Component {
  render() {
    const styles = {
      layout: {
        display: "flex"
      },
      menu: {
        width: "250px"
      },
      contentbox: {
        padding: "20px",
        width: "100%"
      }
    };
    return (
      <div>
        <Header />
        <div style={styles.layout}>
          <Menu styles={styles.menu} />
          <div style={styles.contentbox}>{this.props.children}</div>
        </div>
      </div>
    );
  }
}

export default CMS;
