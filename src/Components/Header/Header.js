import React from "react";

class Header extends React.Component {
  render() {
    const styles = {
      header: {
        background: "#2196f3",
        padding: "15px",
        display: "flex",
        boxShadow: "0px 3px 10px rgba(0,0,0,0.19)"
      },
      logo: {
        color: "#ffffff",
        margin: "0",
        fontSize: "20px"
      }
    };
    return (
      <header style={styles.header}>
        <h1 style={styles.logo}>Hajj People</h1>
      </header>
    );
  }
}

export default Header;
