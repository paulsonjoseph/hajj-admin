import React from "react";
import { Link } from "react-router-dom";
import MenuItem from "@material-ui/core/MenuItem";
import MenuList from "@material-ui/core/MenuList";

class Menu extends React.Component {
  render() {
    return (
      <MenuList style={this.props.styles}>
        <MenuItem component={Link} to="/Dashboard">
          Dashboard
        </MenuItem>
        <MenuItem component={Link} to="/CMS">
          CMS
        </MenuItem>
        <MenuItem>Logout</MenuItem>
      </MenuList>
    );
  }
}

export default Menu;
