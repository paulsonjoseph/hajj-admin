import React from "react";
import MaterialTable from "material-table";

class DataTable extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <MaterialTable
        title={this.props.title}
        columns={this.props.columns}
        data={this.props.data}
      />
    );
  }
}

export default DataTable;
